import sys
import time

from binance.spot import Spot as Client
from prometheus_client import start_http_server, Gauge


spot_client = Client()


def filter_ticker_by_quote_asset_type(asset, list_of_ticker):
    rs = []
    for ticker in list_of_ticker:
        if ticker['symbol'].endswith(asset.upper()):
            rs.append(ticker)
    return rs


def sort_ticker_by_metric(list_of_ticker, metric_name):
    return sorted(list_of_ticker, key = lambda i: float(i[metric_name]), reverse=True)


def get_24hr_ticker():
    return spot_client.ticker_24hr()


def response(list_of_ticker):
    for ticker in list_of_ticker:
        print(ticker['symbol'])


def get_top5_symbols_with_quote_asset_last_24_hr(asset, metric_name):
    all_ticker_info = get_24hr_ticker()
    asset_ticker_info = filter_ticker_by_quote_asset_type(asset, all_ticker_info)
    top_5_ticker = sort_ticker_by_metric(asset_ticker_info, metric_name)[:5]
    return top_5_ticker


def get_orderbook(ticker, limit=200):
    orderbook = spot_client.depth(ticker, limit=limit)
    return orderbook['bids'], orderbook['asks']


def calculate_notional(order_list):
    total = 0
    for i in order_list:
        total = total + float(i[0])*float(i[1])
    return total


def calculate_spread(bids, asks):
    return float(asks[0][0]) - float(bids[0][0])


def q1():
    print('1. Print the top 5 symbols with quote asset BTC and the highest volume over the last 24 hours in descending order.')
    return get_top5_symbols_with_quote_asset_last_24_hr('BTC', 'quoteVolume')


def q2(enable_print=True):
    if enable_print:
        print('2. Print the top 5 symbols with quote asset USDT and the highest number of trades over the last 24 hours in descending order.')
    return get_top5_symbols_with_quote_asset_last_24_hr('USDT', 'count')


def q3(ticker_list):
    print('3. Using the symbols from Q1, what is the total notional value of the top 200 bids and asks currently on each order book?')
    for ticker in ticker_list:
        bids, asks = get_orderbook(ticker['symbol'])
        print('total notional value of top 200 bids of {}: {}'.format(ticker['symbol'], calculate_notional(bids)))
        print('total notional value of top 200 asks of {}: {}'.format(ticker['symbol'], calculate_notional(asks)))


def q4(ticker_list, enable_print=True):
    if enable_print:
        print('4. What is the price spread for each of the symbols from Q2?')
    q4_rs = {}
    for ticker in ticker_list:
        bids, asks = get_orderbook(ticker['symbol'], 1)
        spread = calculate_spread(bids, asks)
        if enable_print:
            print('Price spread of {}: {}'.format(ticker['symbol'], spread))
        q4_rs[ticker['symbol']] = spread
    return q4_rs


def q5(ticker_list):
    print('5. Every 10 seconds print the result of Q4 and the absolute delta from the previous value for each symbol.')
    last_rs = {}
    while True:
        q4_rs = q4(ticker_list, enable_print=False)
        for k, v in q4_rs.items():
            delta = float(v) - float(last_rs.get(k, 0))
            print('Price spread of {}: {}. Delta: {}'.format(k, v, delta))
        last_rs = q4_rs
        time.sleep(10)


def q6():
    print('6. Make the output of Q5 accessible by querying http://localhost:8080/metrics using the Prometheus Metrics format.')
    ticker_list = q2(enable_print=False)
    # declaration of the gauge
    g1= Gauge('price_spread', 'Price spread of the ticker', ['ticker'])
    g2= Gauge('price_delta', 'Price delta of the ticker', ['ticker'])
    port = 8080
    start_http_server(port)
    print('http://localhost:8080/metrics is ready to query')
    last_rs = {}
    while True:
        q4_rs = q4(ticker_list, enable_print=False)
        for k, v in q4_rs.items():
            delta = float(v) - float(last_rs.get(k, 0))
            g1.labels(ticker=k).set(v)
            g2.labels(ticker=k).set(delta)
        last_rs = q4_rs
        time.sleep(10)


def q1_q5():
    q1_rs = q1()
    response(q1_rs)
    q2_rs = q2()
    response(q2_rs)
    q3(q1_rs)
    q4(q2_rs)
    q5(q2_rs)


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    args = sys.argv
    if len(args) != 2:
        print('Please only specify get_answer or start_prometheus in the command. For example: python main.py get_answer or python main.py start_prometheus')
    elif args[1] == 'get_answer':
        q1_q5()
    elif args[1] == 'start_prometheus':
        q6()
    else:
        print('Please only specify get_answer or start_prometheus in the command. For example: python main.py get_answer or python main.py start_prometheus')
