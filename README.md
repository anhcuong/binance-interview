# Prerequisite

- python3
- virtualenv

# Install

```bash
python3 -m venv ~/venv/binance
source ~/venv/binance/bin/activate
pip install binance-connector==1.11.0 prometheus-client==0.13.1
```

# How to run

- To get the answer from q1 to q5, run the below command

```bash
source ~/venv/binance/bin/activate
python main.py get_answer
```

![q1-q5](q1q5.png)

- To launch the prometheus metrics server for q6, run the below command

```bash
source ~/venv/binance/bin/activate
python main.py start_prometheus
```

![q6](q6.png)
